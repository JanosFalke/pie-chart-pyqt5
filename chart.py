import sys
import os
import random
import pickle
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *




class myHisto(QLabel):

    def __init__(self, parent=None):
        QLabel.__init__(self, parent=parent)
        #print('Constructeur de la classe myHisto')
        self.m_list = []

    def max(self):
        bin_max = 0
        for i in self.m_list:
            if i.m_amount > bin_max:
                bin_max = i.m_amount
        return bin_max


class Intervalle:
    def __init__(self, a=0, b=0):
        #print('Constructeur de la classe Intervalle')
        self.m_x = a
        self.m_amount = b


class MyMainWindow(QMainWindow):
    _bins = 10
    _colours = []

    def __init__(self, parent=None):
        super(MyMainWindow,self).__init__()

        # attributs de la fenetre principale
        self.setGeometry(300, 300, 600, 450)
        self.setWindowTitle('Main Window')
        # Barre de status pour afficher les infos
        self.statusBar = QStatusBar()
        self.scene = QGraphicsScene()
        self.view = QGraphicsView(self.scene)
        self.setStatusBar(self.statusBar)
        self.statusBar.showMessage("Zone d'informations, peut toujours servir")
        self.mHisto = myHisto()
        self.couleur = Qt.red
        self.couleurIcon = QPixmap(16, 16)
        self.couleurIcon.fill(self.couleur)
        self.setAcceptDrops(True)
        self.view.setAcceptDrops(True)
        self.createActions()
        self.createMenus()
        self.setCentralWidget(self.view)
        



    def paintEvent(self, *args, **kwargs):
        qp = QPainter()

        qp.begin(self)
        
        if len(self.mHisto.m_list) != 0:
            colours = []
            set_angle = 0
            count1 = 0

            total = 0
            for intervalle in self.mHisto.m_list:
                total += intervalle.m_amount

            for intervalle in self.mHisto.m_list:
                angle = round(float(intervalle.m_amount*5760)/total) 
                ellipse = QGraphicsEllipseItem(0,0,400,400)
                ellipse.setPos(0,0)
                ellipse.setStartAngle(set_angle)
                ellipse.setSpanAngle(angle)
                ellipse.setBrush(self._colours[count1])
                set_angle += angle
                count1 += 1
                self.scene.addItem(ellipse)

        qp.end()

    def createActions(self):
        ####File Menu
        #Bye
        self.exitAct = QAction("&Bye...", self)
        self.exitAct.setShortcut("Ctrl+B")
        self.exitAct.triggered.connect(self.exit)

        #Open
        self.openAct = QAction("&Open...", self)
        self.openAct.setShortcut("Ctrl+O")
        self.openAct.triggered.connect(self.open)

        #Save
        self.saveAct = QAction("&Save...", self)
        self.saveAct.setShortcut("Ctrl+S")
        self.saveAct.triggered.connect(self.save)

        #Restore
        self.restoreAct = QAction("&Restore...", self)
        self.restoreAct.setShortcut("Ctrl+R")
        self.restoreAct.triggered.connect(self.restore)

        ####Display Menu
        #Clear
        self.clearAct = QAction("&Clear...", self)
        self.clearAct.setShortcut("Ctrl+C")
        self.clearAct.triggered.connect(self.clear)


    def createMenus(self):
        #File Menu
        fileMenu = self.menuBar().addMenu("&File")
        fileMenu.addAction(self.openAct)
        fileMenu.addAction(self.saveAct)
        fileMenu.addAction(self.restoreAct)
        fileMenu.addAction(self.exitAct)

        #Display Menu
        displayMenu = self.menuBar().addMenu("&Display")
        displayMenu.addAction(self.clearAct)

    def exit(self):
        self.statusBar.showMessage("Invoked File|Bye ...")
        QApplication.quit()

    def save(self):
        pickle.dump(self.mHisto.m_list, open( "saveHisto.bin", "wb" ) )
        self.statusBar.showMessage("Histogram saved !")

    def open(self):
        fileName, _ = QFileDialog.getOpenFileName(self, "Open File", os.getcwd(), "*.dat")
        self.chargerDonnees(fileName)
        self.statusBar.showMessage("Histogram opened !")

    def restore(self):
        self.mHisto.m_list = pickle.load( open( "saveHisto.bin", "rb" ) )
        self.statusBar.showMessage("Histogram restored !")
        self.setCouleurs()
        self.update()

    def clear(self):
        self.scene.clear()
        self.mHisto.m_list.clear()

        self.statusBar.showMessage("Histogram cleared !")
        self.update()


    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        for url in event.mimeData().urls():
            filename = url.toLocalFile()
            self.statusBar.showMessage(filename)
            self.chargerDonnees(filename)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_R:
            self.mHisto.m_list.clear()

            for i in range(self._bins):
                dataBin = i
                dataAmount = random.randrange(0, 100)
                self.mHisto.m_list.append(Intervalle(int(dataBin), int(dataAmount)))
            self.setCouleurs()
            self.update()


    def chargerDonnees(self, fileName):
        if fileName:
            self.mHisto.m_list.clear() #remettre l'histogramme à vide
            fichier = open(fileName, 'r') #ouvrir le fichier
            ligne = fichier.readline() #lire le fichier

            while ligne:
                dataBin, dataAmount = ligne.split(" ")
                self.mHisto.m_list.append(Intervalle(int(dataBin), int(dataAmount)))
                ligne = fichier.readline() #prochaine ligne
            
            self.setCouleurs()
            

        self.update()

    def setCouleurs(self):
        for count in range(len(self.mHisto.m_list)):
                number = []
                for count in range(3):
                    number.append(random.randrange(0, 255))
                self._colours.append(QColor(number[0],number[1],number[2]))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyMainWindow()
    w.show()
    app.exec_()
